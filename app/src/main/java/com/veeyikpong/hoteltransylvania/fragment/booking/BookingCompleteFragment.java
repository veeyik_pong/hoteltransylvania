package com.veeyikpong.hoteltransylvania.fragment.booking;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.veeyikpong.hoteltransylvania.R;
import com.veeyikpong.hoteltransylvania.activity.booking.BookingActivity;
import com.veeyikpong.hoteltransylvania.fragment.BaseFragment;

import butterknife.BindView;

public class BookingCompleteFragment extends BaseFragment {

    @BindView(R.id.lottie_done)
    protected LottieAnimationView lottie_done;
    @BindView(R.id.tv_bookingNumber)
    protected TextView tv_bookingNumber;
    @BindView(R.id.btn_done)
    protected Button btn_done;

    private Context mContext;
    private Bundle bundle;

    @Override
    public void init() {
        bundle = getArguments();
        if(bundle!=null){
            tv_bookingNumber.setText(getString(R.string.booking_number) + " " + bundle.getString("BookingID"));
            tv_bookingNumber.setVisibility(View.VISIBLE);
        }else{
            tv_bookingNumber.setVisibility(View.GONE);
        }

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BookingActivity)mContext).finish();
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_booking_complete;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
