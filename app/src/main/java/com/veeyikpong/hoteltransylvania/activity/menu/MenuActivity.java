package com.veeyikpong.hoteltransylvania.activity.menu;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import com.veeyikpong.hoteltransylvania.R;
import com.veeyikpong.hoteltransylvania.activity.BaseActivity;
import com.veeyikpong.hoteltransylvania.fragment.menu.MenuFragment;

public class MenuActivity extends BaseActivity {

    private FragmentManager fragmentManager;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void init() {
        fragmentManager = getSupportFragmentManager();

        showFragment(getString(R.string.app_name),new MenuFragment());
    }

    public void showFragment(String title, Fragment fragment) {
        setTitle(title);
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out, R.anim.left_in, R.anim.right_out);
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.press_back_again_to_exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
