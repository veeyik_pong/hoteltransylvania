package com.veeyikpong.hoteltransylvania.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veeyikpong.hoteltransylvania.R;
import com.veeyikpong.hoteltransylvania.pojo.Menu;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Menu> menuArrayList;

    public MenuAdapter(Context c, ArrayList<Menu> menuArrayList) {
        mContext = c;
        this.menuArrayList = menuArrayList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout itemView;
        private ImageView img_icon;
        private TextView tv_title;

        public ViewHolder(View v) {
            super(v);

            itemView = (LinearLayout)v.findViewById(R.id.itemView);
            img_icon = (ImageView) v.findViewById(R.id.img_icon);
            tv_title = (TextView) v.findViewById(R.id.tv_menu_title);
        }
    }

    @NonNull
    @Override
    public MenuAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.ViewHolder holder, final int position) {
        holder.img_icon.setImageResource(menuArrayList.get(position).getIcon());
        holder.tv_title.setText(menuArrayList.get(position).getTitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext,menuArrayList.get(position).getTargetClass());
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuArrayList.size();
    }
}
