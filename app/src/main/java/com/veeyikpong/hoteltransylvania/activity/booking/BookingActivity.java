package com.veeyikpong.hoteltransylvania.activity.booking;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import com.veeyikpong.hoteltransylvania.R;
import com.veeyikpong.hoteltransylvania.activity.BaseActivity;
import com.veeyikpong.hoteltransylvania.fragment.booking.BookingFragment;

public class BookingActivity extends BaseActivity {

    private FragmentManager fragmentManager;

    @Override
    public void init() {
        showBackButton(true);
        fragmentManager = getSupportFragmentManager();

        showFragment(getString(R.string.menu_new_booking), new BookingFragment());
    }

    public void showFragment(String title, Fragment fragment) {
        setTitle(title);
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out, R.anim.left_in, R.anim.right_out);
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
