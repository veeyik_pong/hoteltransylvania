package com.veeyikpong.hoteltransylvania.fragment.menu;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.veeyikpong.hoteltransylvania.R;
import com.veeyikpong.hoteltransylvania.activity.booking.BookingActivity;
import com.veeyikpong.hoteltransylvania.activity.history.HistoryActivity;
import com.veeyikpong.hoteltransylvania.adapter.MenuAdapter;
import com.veeyikpong.hoteltransylvania.fragment.BaseFragment;
import com.veeyikpong.hoteltransylvania.pojo.Menu;

import java.util.ArrayList;

import butterknife.BindView;

public class MenuFragment extends BaseFragment {

    @BindView(R.id.rv_menu)
    protected RecyclerView rv_menu;

    private ArrayList<Menu> menuArrayList;
    private MenuAdapter menuAdapter;

    private Context mContext;

    @Override
    public void init() {
        menuArrayList = new ArrayList<>();
        menuArrayList.add(new Menu(R.drawable.ic_hotel_64dp,getString(R.string.menu_new_booking),BookingActivity.class));
        menuArrayList.add(new Menu(R.drawable.ic_history_64dp,getString(R.string.menu_booking_history),HistoryActivity.class));

        rv_menu.setLayoutManager(new GridLayoutManager(mContext, 2));
        menuAdapter = new MenuAdapter(mContext,menuArrayList);
        rv_menu.setAdapter(menuAdapter);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_menu;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
