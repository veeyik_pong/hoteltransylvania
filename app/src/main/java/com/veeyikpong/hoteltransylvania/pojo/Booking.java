package com.veeyikpong.hoteltransylvania.pojo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "booking")
public class Booking {
    public Booking(){}

    @PrimaryKey(autoGenerate = true)
    private int bookingID;

    @ColumnInfo(name = "adults")
    private int adults;

    @ColumnInfo(name = "children")
    private int children;

    @ColumnInfo(name = "infants")
    private int infants;

    @ColumnInfo(name = "rooms")
    private int rooms;

    public int getAdults() {
        return adults;
    }

    public void setAdults(int adults) {
        this.adults = adults;
    }

    public int getChildren() {
        return children;
    }

    public void setChildren(int children) {
        this.children = children;
    }

    public int getInfants() {
        return infants;
    }

    public void setInfants(int infants) {
        this.infants = infants;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }
}
