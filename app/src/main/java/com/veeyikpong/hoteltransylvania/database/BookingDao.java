package com.veeyikpong.hoteltransylvania.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.veeyikpong.hoteltransylvania.pojo.Booking;

import java.util.List;

@Dao
public interface BookingDao {
    @Query("SELECT * FROM `booking` ORDER BY bookingID DESC")
    List<Booking> getAll();

    @Query("SELECT COUNT(*) FROM `booking`")
    int count();

    @Insert
    long insert(Booking booking);

    @Insert
    void insert(Booking... bookings);

    @Query("DELETE FROM `booking` WHERE bookingID = :id")
    void delete(int id);

    @Query("DELETE FROM `booking`")
    void deleteAll();
}
