package com.veeyikpong.hoteltransylvania.pojo;

public class Menu {
    private int icon;
    private String title;
    private Class<?> targetClass;

    public Menu(int icon, String title, Class<?> targetClass){
        this.icon = icon;
        this.title = title;
        this.targetClass = targetClass;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Class<?> getTargetClass() {
        return targetClass;
    }

    public void setTargetClass(Class<?> targetClass) {
        this.targetClass = targetClass;
    }
}
