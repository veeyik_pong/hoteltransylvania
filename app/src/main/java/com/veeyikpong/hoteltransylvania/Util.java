package com.veeyikpong.hoteltransylvania;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class Util {
    public static void showAlertDialog(Context context, String title, String message){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alertdialog);

        TextView tv_title = (TextView) dialog.findViewById(R.id.alertdialog_title);
        tv_title.setText(title);

        TextView tv_message = (TextView) dialog.findViewById(R.id.alertdialog_message);
        tv_message.setText(message);

        Button dialogButton = (Button) dialog.findViewById(R.id.alertdialog_button);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
