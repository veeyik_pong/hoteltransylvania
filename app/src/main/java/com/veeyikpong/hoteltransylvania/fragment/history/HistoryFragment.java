package com.veeyikpong.hoteltransylvania.fragment.history;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.veeyikpong.hoteltransylvania.R;
import com.veeyikpong.hoteltransylvania.adapter.HistoryAdapter;
import com.veeyikpong.hoteltransylvania.database.HotelTransylvaniaDatabase;
import com.veeyikpong.hoteltransylvania.fragment.BaseFragment;
import com.veeyikpong.hoteltransylvania.pojo.Booking;

import java.util.List;

import butterknife.BindView;

public class HistoryFragment extends BaseFragment {

    @BindView(R.id.rv_history)
    protected RecyclerView rv_history;
    @BindView(R.id.rl_emptyView)
    protected RelativeLayout rl_emptyView;

    private List<Booking> bookingArrayList;
    private HistoryAdapter historyAdapter;

    private Context mContext;

    @Override
    public void init() {
        bookingArrayList = HotelTransylvaniaDatabase.getAppDatabase(mContext).bookingDao().getAll();

        if(bookingArrayList.size() == 0){
            rl_emptyView.setVisibility(View.VISIBLE);
            return;
        }

        rl_emptyView.setVisibility(View.GONE);
        rv_history.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        historyAdapter = new HistoryAdapter(mContext,bookingArrayList);
        rv_history.setAdapter(historyAdapter);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_history;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
