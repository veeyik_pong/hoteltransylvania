package com.veeyikpong.hoteltransylvania.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veeyikpong.hoteltransylvania.R;
import com.veeyikpong.hoteltransylvania.pojo.Booking;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private Context mContext;
    private List<Booking> bookingArrayList;

    public HistoryAdapter(Context c, List<Booking> bookingArrayList) {
        mContext = c;
        this.bookingArrayList = bookingArrayList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout ll_rooms;
        private LinearLayout ll_adults;
        private LinearLayout ll_children;
        private LinearLayout ll_infants;
        private TextView tv_bookingNumber;
        private TextView tv_rooms;
        private TextView tv_adults;
        private TextView tv_children;
        private TextView tv_infants;

        public ViewHolder(View v) {
            super(v);

            ll_rooms = (LinearLayout)v.findViewById(R.id.ll_rooms);
            ll_adults = (LinearLayout)v.findViewById(R.id.ll_adults);
            ll_children = (LinearLayout)v.findViewById(R.id.ll_children);
            ll_infants = (LinearLayout)v.findViewById(R.id.ll_infants);
            tv_bookingNumber = (TextView)v.findViewById(R.id.tv_bookingNumber);
            tv_rooms = (TextView) v.findViewById(R.id.tv_rooms);
            tv_adults = (TextView) v.findViewById(R.id.tv_adults);
            tv_children = (TextView) v.findViewById(R.id.tv_children);
            tv_infants = (TextView) v.findViewById(R.id.tv_infants);
        }
    }

    @NonNull
    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking_history, parent, false);
        return new HistoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryAdapter.ViewHolder holder, int position) {
        Booking booking = bookingArrayList.get(position);

        holder.tv_bookingNumber.setText(mContext.getString(R.string.booking_number) + " HT" + String.valueOf(booking.getBookingID()));
        holder.tv_rooms.setText(String.valueOf(booking.getRooms()) + " " + mContext.getString(R.string.rooms));
        holder.tv_adults.setText(String.valueOf(booking.getAdults()) + " " + mContext.getString(R.string.adults));

        if(booking.getChildren()<=0){
            holder.ll_children.setVisibility(View.GONE);
        }else{
            holder.tv_children.setText(String.valueOf(booking.getChildren()) + " " + mContext.getString(R.string.children));
        }

        if(booking.getInfants() <= 0){
            holder.ll_infants.setVisibility(View.GONE);
        }else{
            holder.tv_infants.setText(String.valueOf(booking.getInfants()) + " " + mContext.getString(R.string.infants));
        }
    }

    @Override
    public int getItemCount() {
        return bookingArrayList.size();
    }
}
