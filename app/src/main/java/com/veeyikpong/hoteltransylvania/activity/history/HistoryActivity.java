package com.veeyikpong.hoteltransylvania.activity.history;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import com.veeyikpong.hoteltransylvania.R;
import com.veeyikpong.hoteltransylvania.activity.BaseActivity;
import com.veeyikpong.hoteltransylvania.fragment.history.HistoryFragment;

public class HistoryActivity extends BaseActivity {

    private FragmentManager fragmentManager;

    @Override
    public void init() {
        fragmentManager = getSupportFragmentManager();

        showBackButton(true);
        showFragment(getString(R.string.menu_booking_history), new HistoryFragment());
    }

    public void showFragment(String title, Fragment fragment) {
        setTitle(title);
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        transaction.setCustomAnimations(R.anim.right_in, R.anim.left_out, R.anim.left_in, R.anim.right_out);
        transaction.replace(R.id.fragmentContainer, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
