# Hotel Transylvania

This is a hotel booking app written in Android Java.

# Key Features

  - Submit a new booking, which will be inserted to local database
  - Check your booking history

# Libraries
- [MaterialSpinner](https://github.com/jaredrummler/MaterialSpinner)
- [Butterknife](http://jakewharton.github.io/butterknife/) for views injection
- [Lottie](https://github.com/airbnb/lottie-android) for some cool animations ;)
- [Room database](https://developer.android.com/topic/libraries/architecture/room)

![Screenshot](https://i.imgur.com/gYe5fjzl.jpg)
![Screenshot](https://i.imgur.com/sNiAkxal.jpg)
![Screenshot](https://i.imgur.com/UQMyNldl.jpg)