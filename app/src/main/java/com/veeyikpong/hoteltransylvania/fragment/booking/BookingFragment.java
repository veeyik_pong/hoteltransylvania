package com.veeyikpong.hoteltransylvania.fragment.booking;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.veeyikpong.hoteltransylvania.R;
import com.veeyikpong.hoteltransylvania.Util;
import com.veeyikpong.hoteltransylvania.activity.booking.BookingActivity;
import com.veeyikpong.hoteltransylvania.database.HotelTransylvaniaDatabase;
import com.veeyikpong.hoteltransylvania.fragment.BaseFragment;
import com.veeyikpong.hoteltransylvania.pojo.Booking;

import butterknife.BindView;
import butterknife.OnClick;

public class BookingFragment extends BaseFragment {

    @BindView(R.id.spinner_adults)
    protected MaterialSpinner spinner_adults;
    @BindView(R.id.spinner_children)
    protected MaterialSpinner spinner_children;
    @BindView(R.id.spinner_infants)
    protected MaterialSpinner spinner_infants;
    @BindView(R.id.tv_rooms)
    protected TextView tv_rooms;

    private Context mContext;

    //Minimum Adult is set to 1
    private Integer[] adultOptions = new Integer[]{1,2,3,4,5,6,7,8,9,10};
    //Since maximum number of rooms per booking is 3, the number of allowed children / infants is limited to 9
    private Integer[] childInfantsOptions = new Integer[]{0,1,2,3,4,5,6,7,8,9};

    private int selectedAdults = 2;
    private int selectedChildren = 0;
    private int selectedInfants = 0;

    @Override
    public void init() {
        spinner_adults.setItems(adultOptions);
        spinner_children.setItems(childInfantsOptions);
        spinner_infants.setItems(childInfantsOptions);

        //Default to 2 adults, since this is the most common selection
        spinner_adults.setSelectedIndex(1);
        //Default to 1 room
        tv_rooms.setText("1");

        spinner_adults.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectedAdults = (int) item;
                refreshRoomNumber();
            }
        });

        spinner_children.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectedChildren = (int) item;
                refreshRoomNumber();
            }
        });

        spinner_infants.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectedInfants = (int) item;
                refreshRoomNumber();
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_booking;
    }

    private void refreshRoomNumber(){
        //Find maximum values among adults, children and infants
        //Since each room can have the same number (3) of adults, children and infants, we just calculate the room based on the maximum value
        int max = Math.max(Math.max(selectedAdults,selectedChildren),selectedInfants);

        //If result smaller than 1, means 1 room needed
        //If between 1 and 2, means 2 rooms needed
        //If between 2 and 3, means 3 rooms needed
        double roomNeeded = max / 3.0;

        if(roomNeeded<1){
            roomNeeded = 1;
        }

        if(roomNeeded>1 && roomNeeded<2){
            roomNeeded = 2;
        }

        if(roomNeeded>2 && roomNeeded<3){
            roomNeeded = 3;
        }

        //Since max room per booking is 3 / Max guests is 7, we will limit the maximum room number to 3
        if(roomNeeded>3){
            roomNeeded = 3;
        }

        tv_rooms.setText(String.valueOf((int)roomNeeded));
    }

    private boolean validate(){
        //Check whether total guests exceeded 7
        int totalGuest = selectedAdults + selectedChildren;
        if(totalGuest > 7){
            Util.showAlertDialog(mContext,getString(R.string.err_title),getString(R.string.err_maximum_guest));
            return false;
        }

        //Check for rooms without adult
        int rooms = Integer.parseInt(tv_rooms.getText().toString());
        if(rooms>selectedAdults){
            Util.showAlertDialog(mContext,getString(R.string.err_title),getString(R.string.err_no_adults_in_room));
            return false;
        }

        return true;
    }

    @OnClick({R.id.btn_confirm, R.id.tv_terms})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_confirm:
                if(!validate())
                    return;

                //Validated, insert into local database
                //Create a new booking object
                Booking booking = new Booking();
                booking.setAdults(selectedAdults);
                booking.setChildren(selectedChildren);
                booking.setInfants(selectedInfants);
                booking.setRooms(Integer.parseInt(tv_rooms.getText().toString()));

                //Get booking ID inserted, pass to next page
                long bookingID = HotelTransylvaniaDatabase.getAppDatabase(mContext).bookingDao().insert(booking);
                Bundle bundle = new Bundle();
                bundle.putString("BookingID","HT"+bookingID);
                BookingCompleteFragment bookingCompleteFragment = new BookingCompleteFragment();
                bookingCompleteFragment.setArguments(bundle);
                ((BookingActivity)mContext).showFragment(getString(R.string.completed), bookingCompleteFragment);
                break;
            case R.id.tv_terms:
                Util.showAlertDialog(mContext,getString(R.string.tnc),getString(R.string.notes));
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
